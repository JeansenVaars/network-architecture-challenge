package org.algosaif.challenge.bank;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Bank DB Model
 * name STRING
 * identifier LONG
 */
@Entity
@Table
public class Bank {

    @Id
    @GeneratedValue(generator = "bank_generator")
    @SequenceGenerator(
            name = "bank_generator",
            sequenceName = "bank_sequence",
            initialValue = 0
    )
    private Long id;

    @NotBlank
    @Size(min = 3, max = 100)
    @Column(name = "name", nullable = false, updatable = true)
    private final String name;

    @Column(name = "identifier", nullable = false, updatable = false)
    private final Long identifier;

    public Bank(String name, Long identifier) {
        this.name = name;
        this.identifier = identifier;
    }

    public Bank() {
        this.name = "<EMPTY>";
        this.identifier = 0L;
    }

    public String getName() {
        return name;
    }

    public Long getIdentifier() {
        return identifier;
    }

}
