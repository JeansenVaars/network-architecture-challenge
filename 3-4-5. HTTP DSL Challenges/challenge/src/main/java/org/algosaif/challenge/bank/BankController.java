package org.algosaif.challenge.bank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * BankController will handle requests to bank information
 */
@RestController
public class BankController {

    @Autowired
    private BankRepository bankRepository;

    @GetMapping("/banks")
    public List<Bank> queryBanks() {
        return bankRepository.findAll();
    }

    @GetMapping("/banks/{identifier}")
    public List<Bank> queryBanksIdentifier(@PathVariable Long identifier) {
        return bankRepository.getBankByIdentifier(identifier);
    }

    @GetMapping("/bank/{identifier}")
    public Bank queryBankIdentifier(@PathVariable Long identifier) {
        List<Bank> results = bankRepository.getBankByIdentifier(identifier);
        if (results.isEmpty()) {
            throw new BankNotFoundException(identifier);
        } else {
            return results.get(0);
        }
    }

}
