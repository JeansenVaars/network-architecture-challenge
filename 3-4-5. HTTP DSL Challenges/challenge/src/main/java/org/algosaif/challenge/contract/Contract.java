package org.algosaif.challenge.contract;

/**
 * Contract processing, simply class
 */
public class Contract {

    private final long id;
    private final String type;

    public Contract(long id, String type) {
        this.id = id;
        this.type = type;
    }

    public long getId() {
        return this.id;
    }

    public String getType() {
        return this.type;
    }

}
