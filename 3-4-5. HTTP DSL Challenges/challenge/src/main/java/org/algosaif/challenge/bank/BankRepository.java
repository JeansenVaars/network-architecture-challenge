package org.algosaif.challenge.bank;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Interface to generate model access to DB
 */
public interface BankRepository extends JpaRepository<Bank, Long> {
    List<Bank> getBankByIdentifier(Long identifier);
}
