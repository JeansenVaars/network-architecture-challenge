package org.algosaif.challenge.user;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Represents request endpoints to User information
 */
@RestController
public class UserController {

    private static final String template = "Processed %s";
    private final AtomicLong counter = new AtomicLong();

    /** \/users\/(\w+)\/info\/(\w+) */
    @GetMapping("/users/{name}/info/{info}")
    public HashMap<String, String> queryUser(@PathVariable String name, @PathVariable String info) {
        System.out.println(String.format("\nLooking up %s", name));
        if (Users.exists(name)) {
            HashMap<String, String> query = new HashMap<>(2);
            query.put("user", Users.get(name).getName());
            query.put(info, Users.get(name).getInfoOrElse(info, "<undefined>"));
            return query;
        } else {
            throw new UserNotFoundException(name);
        }
    }

}
