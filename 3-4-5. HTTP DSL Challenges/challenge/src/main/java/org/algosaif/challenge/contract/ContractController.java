package org.algosaif.challenge.contract;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.*;

/**
 * ContractController to process names dynamically through DSL
 */
@RestController
public class ContractController {

    private static final String template = "Processed %s";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/contract/{name}")
    public Contract processContract(@PathVariable String name) {
        System.out.println(String.format("\nProcessed %s", name));
        return new Contract(counter.incrementAndGet(), name);
    }

}
