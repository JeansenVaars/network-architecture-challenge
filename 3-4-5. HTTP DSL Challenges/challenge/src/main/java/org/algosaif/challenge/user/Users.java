package org.algosaif.challenge.user;

import java.util.HashMap;

/**
 * Singleton handling representing User storage
 */
public class Users {

    private HashMap<String, User> users;

    private Users() {
        HashMap<String, User> userInfo = new HashMap<>(2);

        User maria = new User("Maria");
        maria.addInfo("location", "Spain");
        maria.addInfo("birthday", "11/21/1981");

        User marcelo = new User("Marcelo");
        marcelo.addInfo("location", "UK");
        marcelo.addInfo("birthday", "4/16/1998");

        userInfo.put("Maria", maria);
        userInfo.put("Marcelo", marcelo);

        this.users = userInfo;
    }

    private static final Users usersLoader = new Users();

    public static HashMap<String, User> getAll() {
        return usersLoader.users;
    }

    public static User get(String name) {
        return usersLoader.users.get(name);
    }

    public static boolean exists(String name) {
        return usersLoader.users.containsKey(name);
    }

}
