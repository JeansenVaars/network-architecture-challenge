package org.algosaif.challenge;

import org.algosaif.challenge.bank.Bank;
import org.algosaif.challenge.bank.BankCSVReader;
import org.algosaif.challenge.bank.BankRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.List;

/**
 * Spring boot main application runner, wires to database dynamically
 */
@SpringBootApplication
public class ChallengeApplication implements CommandLineRunner {

	@Autowired
	BankRepository bankRepository;

	public static void main(String args[]) {
		SpringApplication.run(ChallengeApplication.class, args);
	}

	public void run(String... strings) throws IOException {

		System.out.println("\nInitializing CSV Reader, loading banks from file 'banks.csv' into DB\n");

		List<Bank> banks = new BankCSVReader(";", true).read("./banks.csv");

		bankRepository.saveAll(banks);

		System.out.println("\nFetching bank example from Database: 10040000L\n");

		List<Bank> retrievedBanks = bankRepository.getBankByIdentifier(10040000L);

		for (Bank bank : retrievedBanks) {
			System.out.println("\nRETRIEVED -- Bank (Example): " + bank.getName() + " identifier: " + bank.getIdentifier());
		}

		System.out.println("\n\nStarted server, for challenges 3 - 4 - 5. Ready to receive HTTP Calls...\n");

		System.out.println("\n\nExamples:" +
				"\nhttp://localhost:8080/contract/electricity" +
				"\nhttp://localhost:8080/users/Maria/info/location" +
				"\nhttp://localhost:8080/bank/10040000" +
				"\nhttp://localhost:8080/banks\n");

	}

}
