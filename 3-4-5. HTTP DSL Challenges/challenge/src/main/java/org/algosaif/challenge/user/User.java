package org.algosaif.challenge.user;

import java.util.HashMap;

/**
 * User representation, info is a dynamic source of profile information
 */
public class User {

    private final String name;
    private HashMap<String, String> info;

    public User(String name) {
        this.name = name;
        this.info = new HashMap<>(10);
    }

    public User(String name, HashMap<String, String> info) {
        this.name = name;
        this.info = info;
    }

    public String getName() {
        return this.name;
    }

    public void addInfo(String key, String value) {
        this.info.put(key, value);
    }

    public String getInfoOrElse(String key, String defaultValue) {
        return this.info.getOrDefault(key, defaultValue);
    }

}
