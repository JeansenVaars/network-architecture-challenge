package org.algosaif.challenge.user;

/**
 * Handle exceptions for failure
 */
public class UserNotFoundException extends RuntimeException {

    UserNotFoundException(String name) {
        super("Error Code 404. Could not find user: " + name);
    }

}
