package org.algosaif.challenge.bank;

/**
 * Bank not found exception handling
 */
public class BankNotFoundException extends RuntimeException {
    BankNotFoundException(Long identifier) {
        super("Error Code 404. Could not find bank by identifier: " + identifier.toString());
    }
}
