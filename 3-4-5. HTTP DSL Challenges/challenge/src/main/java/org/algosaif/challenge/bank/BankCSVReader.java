package org.algosaif.challenge.bank;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Handle CSV reading for input processing
 */
public class BankCSVReader {

    private String delimiter = ",";
    private boolean skipHeader = true;

    public BankCSVReader() {}

    public BankCSVReader(String delimiter, boolean skipHeader) {
        this.delimiter = delimiter;
        this.skipHeader = skipHeader;
    }

    public List<Bank> read(String path) throws IOException {
        List<Bank> records = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(path));
        String line;
        if (skipHeader) br.readLine();
        while ((line = br.readLine()) != null) {
            String[] values = line.split(this.delimiter);
            if (values.length == 2) {
                records.add(new Bank(values[0], Long.parseLong(values[1])));
            }
        }
        return records;
    }
}
