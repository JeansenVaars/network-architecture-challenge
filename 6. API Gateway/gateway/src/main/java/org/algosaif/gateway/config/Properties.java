package org.algosaif.gateway.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Skeleton for general Gateway configuration
 */
@ConfigurationProperties(prefix = "algosaif.gateway")
public class Properties {

    public Long getCacheSize() {
        throw new NotImplementedException();
    }

    public void setCacheSize(Long cacheSize) {
        throw new NotImplementedException();
    }

    public void setRetryAmount(int value) {
        throw new NotImplementedException();
    }

    public void getRetryAmount() {
        throw new NotImplementedException();
    }

}
