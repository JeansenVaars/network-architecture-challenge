package org.algosaif.gateway.failover;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Example for HTTP response handling
 */
@ControllerAdvice
public class SomeNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(SomeNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String SomeNotFoundAdvice(SomeNotFoundException ex) {
        return ex.getMessage();
    }

}
