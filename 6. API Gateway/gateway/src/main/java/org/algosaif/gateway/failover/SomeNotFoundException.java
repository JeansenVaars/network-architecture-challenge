package org.algosaif.gateway.failover;

/**
 * Example base for Error handling for specific fail situations
 */
public class SomeNotFoundException extends RuntimeException {

    SomeNotFoundException(String name) {
        super("Error Code 404. Could not find resource: " + name);
    }

}
