package org.algosaif.gateway.state;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Responsible for spawning multiple clones of a request, to get it distributed across endpoints.
 * Also handles aggregation once the response gets back.
 * Acts as a dispatcher
 */
public class SessionSpawnAggregator {
    public SessionSpawnAggregator() {
        throw new NotImplementedException();
    }
}
