package org.algosaif.gateway.cache;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;


/**
 * Fast storage controller to handle information gathering Sessions, Payloads and Common requests,
 * to skip further Gateway processing and return back cached responses
 */
public class L1L2CacheHandler {

    public L1L2CacheHandler() {
        throw new NotImplementedException();
    }

    public void put(String key, Object value) {
        throw new NotImplementedException();
    }

    public Object get(String key) {
        throw new NotImplementedException();
    }

    public void evict(String key) {
        throw new NotImplementedException();
    }

    public void clear() {
        throw new NotImplementedException();
    }

}
